﻿using System.Globalization;

namespace Common.Text
{
    public class CsvWriter : WriterBase
    {
        public CsvWriter() :
            base()
        {
            ColumnSeparator = CultureInfo.CurrentCulture.TextInfo.ListSeparator;
        }

        public CsvWriter(params string[] columns)
            : this()
        {
            this.AddRow(columns);
        }

        public void AddRow(params string[] a_columns)
        {
            int i = 1;
            while (i < a_columns.GetLength(0))
            {
                SBuffer.Append(a_columns[i - 1]);
                SBuffer.Append(ColumnSeparator);
                i++;
            }
            if (a_columns.GetLength(0) > 0)
            {
                SBuffer.AppendLine(a_columns[i - 1]);
            }
        }

        private string ColumnSeparator
        {
            get;
            set;
        }
    }
}
