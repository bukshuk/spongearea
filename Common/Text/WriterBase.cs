﻿using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Common.Text
{
    public enum WriterEncoding
    {
        Ascii,
        Utf8,
        Utf8WithoutBom
    }

    public class WriterBase
    {
        public WriterBase()
        {
            SBuffer = new StringBuilder();
        }

        public void Save(string a_FilePath, WriterEncoding a_Encoding)
        {
            Encoding enc = null;

            switch (a_Encoding)
            {
                case WriterEncoding.Ascii:
                    enc = Encoding.ASCII;
                    break;
                case WriterEncoding.Utf8:
                    enc = Encoding.UTF8;
                    break;
                case WriterEncoding.Utf8WithoutBom:
                    enc = new UTF8Encoding(false);
                    break;
                default:
                    throw new KeyNotFoundException();
            }

            using (StreamWriter sw = new StreamWriter(a_FilePath, false, enc))
            {
                sw.Write(SBuffer.ToString());
            }
        }

        public virtual void Save(string a_FilePath)
        {
            this.Save(a_FilePath, WriterEncoding.Utf8);
        }

        public void AppendLine(string a_FormatString = "")
        {
            SBuffer.AppendLine(a_FormatString);
        }

        public void AppendFormatLine(string a_FormatString = "", params object[] a_Values)
        {
            SBuffer.AppendFormat(a_FormatString, a_Values);
            SBuffer.AppendLine();
        }

        public static bool UnicodeToAscii(ref string a_text)
        {
            bool encoded = false;

            StringBuilder returnText = new StringBuilder();

            Encoding e = new UTF8Encoding();

            byte[] bytes = e.GetBytes(a_text);

            foreach (var b in bytes)
            {
                if (b > 0x7F)
                {
                    returnText.AppendFormat("={0:X2}", b);
                    encoded = true;
                }
                else
                {
                    returnText.Append((char)b);
                }
            }

            a_text = returnText.ToString();

            return encoded;
        }

        protected StringBuilder SBuffer
        {
            get;
            set;
        }
    }
}
