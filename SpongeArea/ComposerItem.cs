﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace SpongeArea
{
    internal class ComposerItem
    {
        public ComposerItem(List<Color> colors)
        {
            this.Colors = new Dictionary<string, int>();
            colors.ForEach(c => this.Colors.Add(ComposerItem.ColorToString(c), 0));
        }

        public string Name
        {
            get;
            set;
        }

        public Dictionary<string, int> Colors
        {
            get;
            set;
        }

        public int Others
        {
            get;
            set;
        }

        public int Dpi
        {
            get;
            set;
        }

        public int Total()
        {
            return this.Colors.Sum(c => c.Value) + this.Others;
        }

        public static string ColorToString(Color color)
        {
            return color.R.ToString() + color.G.ToString() + color.B.ToString();
        }
    }
}
