﻿using NLog;

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Common.Text;

namespace SpongeArea
{
    internal class Composer
    {
        private List<Color> Colors
        {
            get;
            set;
        }

        private ConcurrentStack<ComposerItem> Items
        {
            get;
            set;
        }

        public Logger Logger { get; }

        public Composer(List<Color> colors)
        {
            Colors = new List<Color>(colors);
            
            Items = new ConcurrentStack<ComposerItem>();

            Logger = LogManager.GetCurrentClassLogger();
        }

        public void Play(string sourceFolder, string resultFile, bool doParallel, Action<string> outputCallback)
        {
            this.Items.Clear();
            this.CollectItems(sourceFolder, doParallel, outputCallback);
            this.WriteItems(resultFile);
        }

        private void CollectItems(string folder, bool doParallel, Action<string> outputCallback)
        {
            string[] files = Directory.GetFiles(folder, "*.bmp");

            int totalNumber = files.GetLength(0);
            int count = totalNumber;

            var parallelismDegree = (doParallel) ? Environment.ProcessorCount : 1;

            var options = new ParallelOptions() { MaxDegreeOfParallelism = parallelismDegree };

            Parallel.ForEach(files, options, (bmpFile) =>
            {
                string stateString = "FAILED";

                try
                {
                    this.CollectItemFromFile(bmpFile);

                    stateString = "Succeed";
                }
                finally
                {
                    lock (files)
                    {
                        var message = $"[{count--} of {totalNumber}] {Path.GetFileName(bmpFile)} {stateString}";

                        outputCallback(message);
                        Logger.Trace(message);
                    }
                }
            });
        }

        private void WriteItems(string a_file)
        {
            var excelWriter = new CsvWriter();

            excelWriter.AddRow(this.GetHeaderCells());

            foreach (var item in this.Items.OrderBy(p => p.Name))
            {
                excelWriter.AddRow(this.GetItemCells(item));
            }

            excelWriter.Save(a_file);
        }

        private void CollectItemFromFile(string file)
        {
            var item = new ComposerItem(this.Colors)
            {
                Name = Path.GetFileNameWithoutExtension(file)
            };

            using (var image = new Bitmap(file))
            {
                item.Dpi = Convert.ToInt32(image.HorizontalResolution);

                for (int x = 0; x < image.Width; x++)
                {
                    for (int y = 0; y < image.Height; y++)
                    {
                        var color = image.GetPixel(x, y);
                        var colorKey = ComposerItem.ColorToString(color);

                        if (item.Colors.ContainsKey(colorKey))
                        {
                            item.Colors[colorKey]++;
                        }
                        else
                        {
                            item.Others++;
                        }
                    }
                }
            }

            this.Items.Push(item);
        }

        private string[] GetHeaderCells()
        {
            var headerCells = new List<string>
            {
                "Name"
            };

            headerCells.AddRange(this.Colors.Select(c => c.Name));

            headerCells.Add("Other");
            headerCells.Add("Total");
            headerCells.Add("DPI");

            return headerCells.ToArray();
        }

        private string[] GetItemCells(ComposerItem item)
        {
            var itemCells = new List<string>
            {
                item.Name
            };

            itemCells.AddRange(item.Colors.Select(c => c.Value.ToString()));

            itemCells.Add(item.Others.ToString());
            itemCells.Add(item.Total().ToString());
            itemCells.Add(item.Dpi.ToString());

            return itemCells.ToArray();
        }
    }
}
